﻿using System;
using System.Collections.Generic;
using System.Threading;
using EasyNetQ;
using Parkinc.Mail.DTOs;

namespace Parkinc.Notification.Driver
{
    public class NotificationController
    {
        private static IBus _bus;

        public NotificationController(IBus bus)
        {
            _bus = bus;
        }

        public void Send(MailDTO message)
        {
            Console.WriteLine("Sending an email...");
            _bus.PublishAsync(message);
        }

        public void Send(MailDTO message, int amount, int delay)
        {
            while (amount > 0)
            {
                Console.WriteLine("Sending an email...");

                _bus.PublishAsync(message);
                Thread.Sleep(delay);

                amount--;
            }
        }

        public void SendInfinitly(MailDTO message, int delay)
        {
            while (true)
            {
                Console.WriteLine("Sending an email...");

                _bus.PublishAsync(message);
                Thread.Sleep(delay);
            }
        }

        public MailDTO SampleMessage => new MailDTO()
        {
            ParkingPlaces = new List<ParkingPlaceDTO>
            {
                new ParkingPlaceDTO
                {
                    Name = "Place1",
                    AllParkingPlaces = 100,
                    FreeParkingPlaces = 20
                }
            },
            RecieverEmail = "miropakanec@gmail.com"
        };
    }
}
