﻿using System;
using System.Threading;
using EasyNetQ;
using Parkinc.Mail.DTOs;

namespace Parkinc.Notification.Driver
{
    class Program
    {
        static void Main(string[] args)
        {
            var bus = RabbitHutch.CreateBus("host=localhost");
            var controller = new NotificationController(bus);

            controller.SendInfinitly(controller.SampleMessage, 4000);
        }
    }
}
