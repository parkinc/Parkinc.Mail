﻿using System;
using System.Linq;
using EasyNetQ;
using Moq;
using Parkinc.Mail.Service.Endpoints;
using Parkinc.Mail.Service.Manager;
using Parkinc.Mail.uTests.Utilities;
using Xunit;

namespace Parkinc.Mail.uTests
{
    public class EndpointManagerTests
    {
        [Theory]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(10)]
        public void StartAll_ListOfEndpoints_HappyPath(int numberOfEndpoints)
        {
            //arrange
            var busMock = new Mock<IBus>();
            var endpoint = new Mock<IEndpoint>();
            var endpointList = EndpointManagerHelper.GetEndpoints(numberOfEndpoints, endpoint);

            var sut = new EndpointManager(endpointList, busMock.Object);

            //act
            sut.StartAll();

            //assert
            endpoint.Verify(m => m.Start(), Times.Exactly(numberOfEndpoints));
        }

        [Theory]
        [InlineData(0)]
        public void StartAll_ListOfNullEndpoints_ThrowsException(int numberOfEndpoints)
        {
            //arrange
            var busMock = new Mock<IBus>();
            var endpoint = new Mock<IEndpoint>();
            var endpointList = EndpointManagerHelper.GetEndpoints(numberOfEndpoints, endpoint);

            var sut = new EndpointManager(endpointList, busMock.Object);

            //act
            var exception = Record.Exception(() => sut.StartAll());

            //assert
            Assert.NotNull(exception);
            Assert.IsType<Exception>(exception);
            Assert.Equal("There must be at least one endpoint registered in the endpoint manager.", exception.Message);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(10)]
        public void StartAll_EndpontmanagerHasEndpoints_SetBusIsCalledOnEveryEndpoint(int numberOfEndpoints)
        {
            //arrange
            var busMock = new Mock<IBus>();
            var endpointMock = new Mock<IEndpoint>();
            var endpointList = EndpointManagerHelper.GetEndpoints(numberOfEndpoints, endpointMock);

            var sut = new EndpointManager(endpointList, busMock.Object);

            //act
            sut.StartAll();

            //assert
            endpointMock.Verify(m => m.Setbus(busMock.Object), Times.Exactly(numberOfEndpoints));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(10)]
        public void AddEndpoints_ValidEndpointList_EndpointsAreAdded(int numberOfEndpoints)
        {
            //arrange
            var busMock = new Mock<IBus>();
            var endpointMock = new Mock<IEndpoint>();
            var endpointList = EndpointManagerHelper.GetEndpoints(numberOfEndpoints, endpointMock);

            var sut = new EndpointManager(busMock.Object);

            //act
            sut.AddEndpoints(endpointList);

            //assert
            Assert.Equal(sut.Endpoints.Count(), numberOfEndpoints);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(10)]
        public void AddEndpoints_ValidEndpointList_BusIsSetOnEveryEndpoint(int numberOfEndpoints)
        {
            //arrange
            var busMock = new Mock<IBus>();
            var endpointMock = new Mock<IEndpoint>();
            var endpointList = EndpointManagerHelper.GetEndpoints(numberOfEndpoints, endpointMock);

            var sut = new EndpointManager(busMock.Object);

            //act
            sut.AddEndpoints(endpointList);

            //assert
            endpointMock.Verify(m => m.Setbus(busMock.Object), Times.Exactly(numberOfEndpoints));
        }

        [Fact]
        public void AddEndpoint_ValidEndpoint_EndpointIsAddedToTheExistingList()
        {
            //arrange
            const int numberOfEndpoints = 5;
            var busMock = new Mock<IBus>();
            var endpointMock = new Mock<IEndpoint>();
            var endpointList = EndpointManagerHelper.GetEndpoints(numberOfEndpoints, endpointMock);

            var sut = new EndpointManager(endpointList, busMock.Object);

            //act
            sut.AddEndpoint(endpointMock.Object);

            //assert
            Assert.Equal(numberOfEndpoints + 1, sut.Endpoints.Count());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(5)]
        public void StopAll_ValidNumberOfEndpoints_StopMethodIsCalledOnEveryEndpoint(int numberOfEndpoints)
        {
            //arrange
            var busMock = new Mock<IBus>();
            var endpointMock = new Mock<IEndpoint>();
            var endpointList = EndpointManagerHelper.GetEndpoints(numberOfEndpoints, endpointMock);

            var sut = new EndpointManager(endpointList, busMock.Object);

            //act
            sut.StopAll();

            //assert
            endpointMock.Verify(m => m.Stop(), Times.Exactly(numberOfEndpoints));
        }

        [Theory]
        [InlineData(0)]
        public void StopAll_EmptyList_StopMethodIsCalledOnEveryEndpoint(int numberOfEndpoints)
        {
            //arrange
            var busMock = new Mock<IBus>();
            var endpointMock = new Mock<IEndpoint>();
            var endpointList = EndpointManagerHelper.GetEndpoints(numberOfEndpoints, endpointMock);

            var sut = new EndpointManager(endpointList, busMock.Object);

            //act
            var exception = Record.Exception(() => sut.StopAll());

            //assert
            Assert.NotNull(exception);
            Assert.IsType<Exception>(exception);
            Assert.Equal("There must be at least one endpoint registered in the endpoint manager.", exception.Message);
        }
    }


}
