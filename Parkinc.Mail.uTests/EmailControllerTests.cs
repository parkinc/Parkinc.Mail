﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Parkinc.Mail.Core;
using Parkinc.Mail.Models;
using Parkinc.Mail.uTests.Extensions;
using SendGrid;
using SendGrid.Helpers.Mail;
using Xunit;

namespace Parkinc.Mail.uTests
{
    [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
    public class EmailControllerTests
    {
        private Mock<ISendGridClient> _emailClient;
        private Mock<IAdvertisementController> _adController;
        private EmailController _sut;

        [Fact]
        public async Task Send_InvalidRecieverEmail_ThrowsArgumentException()
        {
            //arrange
            var emailModel = new EmailModel()
            {
                Content = "dummy-content",
                Reciever = "invalid-receiver",
                Sender = "miropakanec@valid.com"
            };

            _emailClient = new Mock<ISendGridClient>();
            _adController = new Mock<IAdvertisementController>();

            _sut = new EmailController(_emailClient.Object, _adController.Object);

            //act
            var exception = await Record.ExceptionAsync(() => _sut.Send(emailModel));

            //assert
            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
            Assert.Equal(exception.Message, "Email objet is not valid.");
        }

        [Fact]
        public async Task Send_InvalidSenderEmail_ThrowsArgumentException()
        {
            //arrange
            var emailModel = new EmailModel()
            {
                Content = "dummy-content",
                Sender = "invalid-sender",
                Reciever = "miropakanec@valid.com"
            };

            _emailClient = new Mock<ISendGridClient>();
            _adController = new Mock<IAdvertisementController>();

            _sut = new EmailController(_emailClient.Object, _adController.Object);

            //act
            var exception = await Record.ExceptionAsync(() => _sut.Send(emailModel));

            //assert
            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
            Assert.Equal(exception.Message, "Email objet is not valid.");
        }

        [Fact]
        public async Task Send_validEmail_HappyPath()
        {
            //arrange
            var emailModel = new EmailModel()
            {
                Content = "dummy-content",
                Sender = "miropakanec@valid.com",
                Reciever = "miropakanec@valid.com"
            };

            _emailClient = new Mock<ISendGridClient>()
                .WithAcceptedSendResponse();

            _adController = new Mock<IAdvertisementController>();

            _sut = new EmailController(_emailClient.Object, _adController.Object);

            //act
            await _sut.Send(emailModel);

            //assert
            _emailClient.Verify(x => x.SendEmailAsync(It.IsAny<SendGridMessage>(), It.IsAny<CancellationToken>()),
                Times.Once);
        }

        [Fact]
        public async Task Send_EmailModelWithAds_AdvertisementControllerIsCalledOnce()
        {
            //arrange
            var emailModel = new EmailModel()
            {
                Content = "dummy-content",
                Sender = "miropakanec@valid.com",
                Reciever = "miropakanec@valid.com",
                WithAds = true
            };

            _emailClient = new Mock<ISendGridClient>();
            _adController = new Mock<IAdvertisementController>();

            _sut = new EmailController(_emailClient.Object, _adController.Object);

            //act
            await _sut.Send(emailModel);

            //assert
            _adController.Verify(x => x.Fetch(), Times.Once);
        }

        [Fact]
        public async Task Send_EmailModelWithoutAds_AdvertisementControllerIsNeverCalled()
        {
            //arrange
            var emailModel = new EmailModel()
            {
                Content = "dummy-content",
                Sender = "miropakanec@valid.com",
                Reciever = "miropakanec@valid.com",
                WithAds = false
            };

            _emailClient = new Mock<ISendGridClient>();
            _adController = new Mock<IAdvertisementController>();

            _sut = new EmailController(_emailClient.Object, _adController.Object);

            //act
            await _sut.Send(emailModel);

            //assert
            _adController.Verify(x => x.Fetch(), Times.Never);
        }
    }
}
