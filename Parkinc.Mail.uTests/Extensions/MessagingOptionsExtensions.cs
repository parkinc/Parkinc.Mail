﻿using Microsoft.Extensions.Options;
using Moq;
using Parkinc.Mail.Models.Configuration;

namespace Parkinc.Mail.uTests.Extensions
{
    public static class MessagingOptionsExtensions
    {
        public static Mock<IOptions<MessagingProviderConfig>> WithValue(
            this Mock<IOptions<MessagingProviderConfig>> model,
            MessagingProviderConfig value)
        {
            model.Setup(x => x.Value).Returns(value);
            return model;
        }
    }
}
