﻿using System.Net;
using System.Threading;
using Moq;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Parkinc.Mail.uTests.Extensions
{
    public static class SendGridClientExtensions
    {
        public static Mock<ISendGridClient> WithAcceptedSendResponse(this Mock<ISendGridClient> mock)
        {
            var response = new Response(HttpStatusCode.Accepted, null, null);

            mock.Setup(x => x.SendEmailAsync(It.IsAny<SendGridMessage>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);

            return mock;
        }

        public static Mock<ISendGridClient> WithServerErrorSendResponse(this Mock<ISendGridClient> mock)
        {
            var response = new Response(HttpStatusCode.InternalServerError, null, null);

            mock.Setup(x => x.SendEmailAsync(It.IsAny<SendGridMessage>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);

            return mock;
        }
    }
}
