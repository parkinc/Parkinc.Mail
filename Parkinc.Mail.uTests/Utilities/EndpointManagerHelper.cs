﻿using System.Collections.Generic;
using Moq;
using Parkinc.Mail.Service.Endpoints;

namespace Parkinc.Mail.uTests.Utilities
{
    internal static class EndpointManagerHelper
    {
        internal static IEnumerable<IEndpoint> GetEndpoints(int amount, Mock<IEndpoint> mock)
        {
            var list = new List<IEndpoint>();

            while (amount > 0)
            {
                list.Add(mock.Object);
                amount--;
            }

            return list;
        }
    }
}
