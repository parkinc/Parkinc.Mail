﻿using System;
using System.Collections.Generic;
using EasyNetQ;
using Microsoft.Extensions.Options;
using Moq;
using Parkinc.Mail.Core;
using Parkinc.Mail.Models.Configuration;
using Parkinc.Mail.Service.Endpoints;
using Parkinc.Mail.uTests.Extensions;
using Xunit;

namespace Parkinc.Mail.uTests
{
    public class MailEndpointTests
    {
        [Fact]
        public void Start_BusIsNull_NullRefferenceException()
        {
            //Arrange
            var emailControllerMock = new Mock<IEmailController>();
            var emailProviderConfigOptionsMock = new Mock<IOptions<EmailProviderConfig>>();     
            var messagingProviderConfigOptionsMock = new Mock<IOptions<MessagingProviderConfig>>();
 
            var sut = new MailEndpoint(emailControllerMock.Object, emailProviderConfigOptionsMock.Object,
                messagingProviderConfigOptionsMock.Object);

            //Act
            var exception = Record.Exception(()=> sut.Start());

            //Assert
            Assert.NotNull(exception);
            Assert.IsType<NullReferenceException>(exception);
            Assert.Equal("Bus cannot be null.", exception.Message);
        }

        [Fact]
        public void Start_ValidProperties_EndpointIsRunning()
        {
            //Arrange
            var emailControllerMock = new Mock<IEmailController>();
            var emailProviderConfigOptionsMock = new Mock<IOptions<EmailProviderConfig>>();

            var endpointConfig = new EndpointConfig()
            {
                Name = "MailEndpoint",
                SubscriptionId = "Parkinc.MailEndpoint.Service.Email"
            };

            var messagingProviderConfig = new MessagingProviderConfig()
            {
                ConnectionString =
                    "host=swan.rmq.cloudamqp.com;virtualhost=naghkhhc;username=naghkhhc;password=uRHTZEgVK7KZKcvrXe1WjgQpqUBK1y_M",
                Endpoints = new List<EndpointConfig>
                {
                    endpointConfig
                }
            };

            var messagingProviderConfigOptionsMock = new Mock<IOptions<MessagingProviderConfig>>()
                .WithValue(messagingProviderConfig);

            var bus = new Mock<IBus>();

            var sut = new MailEndpoint(emailControllerMock.Object, emailProviderConfigOptionsMock.Object,
                messagingProviderConfigOptionsMock.Object, bus.Object);

            //Act
            sut.Start();

            //Assert
            Assert.True(sut.IsRunning());
        }

        [Fact]
        public void Stop_ValidProperties_EndpointIsNotRunning()
        {
            //Arrange
            var emailControllerMock = new Mock<IEmailController>();
            var emailProviderConfigOptionsMock = new Mock<IOptions<EmailProviderConfig>>();
            var messagingProviderConfigOptionsMock = new Mock<IOptions<MessagingProviderConfig>>();
            var bus = new Mock<IBus>();

            var sut = new MailEndpoint(emailControllerMock.Object, emailProviderConfigOptionsMock.Object,
                messagingProviderConfigOptionsMock.Object, bus.Object);

            //Act
            sut.Stop();

            //Assert
            Assert.False(sut.IsRunning());
        }

        [Fact]
        public void SubscriptionId_ValidProperties_HappyPath()
        {
            //Arrange
            var emailControllerMock = new Mock<IEmailController>();
            var emailProviderConfigOptionsMock = new Mock<IOptions<EmailProviderConfig>>();

            var endpointConfig = new EndpointConfig()
            {
                Name = "MailEndpoint",
                SubscriptionId = "Parkinc.MailEndpoint.Service.Email"
            };

            var messagingProviderConfig = new MessagingProviderConfig()
            {
                ConnectionString =
                    "host=swan.rmq.cloudamqp.com;virtualhost=naghkhhc;username=naghkhhc;password=uRHTZEgVK7KZKcvrXe1WjgQpqUBK1y_M",
                Endpoints = new List<EndpointConfig>
                {
                    endpointConfig
                }
            };

            var messagingProviderConfigOptionsMock = new Mock<IOptions<MessagingProviderConfig>>()
                .WithValue(messagingProviderConfig);

            var bus = new Mock<IBus>();

            var sut = new MailEndpoint(emailControllerMock.Object, emailProviderConfigOptionsMock.Object,
                messagingProviderConfigOptionsMock.Object, bus.Object);

            //Act
            var result = sut.SubscriptionId;

            //Assert
            Assert.Equal("Parkinc.MailEndpoint.Service.Email", result);
        }
    }
}

