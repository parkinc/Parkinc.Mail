﻿using System.Collections;
using System.Collections.Generic;

namespace Parkinc.Mail.DTOs
{
    public class MailDTO
    {
        public string RecieverEmail { get; set; }
        public IEnumerable<ParkingPlaceDTO> ParkingPlaces { get; set; }
        public bool WithAds { get; set; }
    }
}
