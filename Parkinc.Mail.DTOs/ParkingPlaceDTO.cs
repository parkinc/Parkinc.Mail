﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.Mail.DTOs
{
    public class ParkingPlaceDTO
    {
        public string Name { get; set; }
        public int FreeParkingPlaces { get; set; }
        public int AllParkingPlaces { get; set; }
    }
}
