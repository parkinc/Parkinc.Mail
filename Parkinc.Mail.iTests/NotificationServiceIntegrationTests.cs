using System.Collections.Generic;
using System.Threading.Tasks;
using EasyNetQ;
using Microsoft.Extensions.Options;
using Moq;
using Parkinc.Mail.Core;
using Parkinc.Mail.Models;
using Parkinc.Mail.Models.Configuration;
using Parkinc.Mail.Service.Endpoints;
using Parkinc.Mail.Service.Manager;
using Parkinc.Mail.uTests.Extensions;
using Parkinc.Notification.Driver;
using Xunit;

namespace Parkinc.Mail.iTests
{
    public class NotificationServiceIntegrationTests
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(10)]
        public async Task SendsValidMessage_MessageisRecieved(int numberOfMessages)
        {
            //arrange
            var bus = Bus;

            var notificationController = new NotificationController(bus);
            var message = notificationController.SampleMessage;

            var controller = new Mock<IEmailController>();

            var emailOptionsMock = EmailOptionsMock;
            var messagingOptionsMock = MessagingOptionsMock;

            var endpoint = new MailEndpoint(controller.Object, emailOptionsMock.Object, messagingOptionsMock.Object);
            
            var endpointList = new List<IEndpoint>()
            {
                endpoint
            };

            var endpointManager = new EndpointManager(endpointList, bus);

            //act
            await Task.Factory.StartNew(() =>
            {
                endpointManager.StartAll();
            });

            await Task.Factory.StartNew(() =>
            {
                notificationController.Send(message, numberOfMessages, 100);
            });

            endpointManager.StopAll();

            //assert
            controller.Verify(x => x.Send(It.IsAny<EmailModel>()), Times.AtLeast(numberOfMessages));
        }

        private static IBus Bus
        {
            get
            {
                const string connectionString =
                    "host=swan.rmq.cloudamqp.com;virtualhost=naghkhhc;username=naghkhhc;password=uRHTZEgVK7KZKcvrXe1WjgQpqUBK1y_M";

                return RabbitHutch.CreateBus(connectionString);
            }
        }

        private static string ConnectionString => "host=swan.rmq.cloudamqp.com;virtualhost=naghkhhc;username=naghkhhc;password=uRHTZEgVK7KZKcvrXe1WjgQpqUBK1y_M";

        private Mock<IOptions<EmailProviderConfig>> EmailOptionsMock
        {
            get
            {
                var emailProviderConfig = new EmailProviderConfig()
                {
                    ApiKey = "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",
                    SenderEmail = "miropakanec@gmail.com"
                };

                var emailOptionsMock = new Mock<IOptions<EmailProviderConfig>>()
                    .WithValue(emailProviderConfig);

                return emailOptionsMock;
            }
        }

        private Mock<IOptions<MessagingProviderConfig>> MessagingOptionsMock
        {
            get
            {
                var endpointConfig = new EndpointConfig()
                {
                    Name = "MailEndpoint",
                    SubscriptionId = "Parkinc.MailEndpoint.Service.Email"
                };

                var messagingProviderConfig = new MessagingProviderConfig()
                {
                    ConnectionString = ConnectionString,
                    Endpoints = new List<EndpointConfig>
                    {
                        endpointConfig
                    }
                };

                var messagingOptionsMock = new Mock<IOptions<MessagingProviderConfig>>()
                    .WithValue(messagingProviderConfig);

                return messagingOptionsMock;
            }
        }
    }
}
