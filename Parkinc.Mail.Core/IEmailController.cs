﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Parkinc.Mail.DTOs;
using Parkinc.Mail.Models;
using SendGrid;

namespace Parkinc.Mail.Core
{
    public interface IEmailController
    {
        Task<Response> Send(EmailModel model);
        string GenerateEmailContent(IEnumerable<ParkingPlaceDTO> parkingPlaces, bool withAds);
    }
}
