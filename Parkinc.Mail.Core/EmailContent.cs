﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Parkinc.Mail.DTOs;

namespace Parkinc.Mail.Core
{
    public static class EmailContent
    {
        public static string GenerateHeader()
        {
            return $@"<div>
                <div>
                <h3>Here is you <b>Parking summary</b></h3>
                <p>for {string.Format("{0:f}", DateTime.Now)}</p>
                <br/>";
        }

        public static string GenerateFooter()
        {
            return @"<br/>
                <p>We wish you a pleasang day.</p>
                <p>Parkinc</p>
                </div>";
        }

        public static string GenerateTable(IEnumerable<ParkingPlaceDTO> parkingPlaces)
        {
            var content = "<table style=\"width:100%; text-align:left\">";
            content += GenerateTableHead();
            content = parkingPlaces.Aggregate(content,
                (current, parkingPlace) => current + GenerateEmailRow(parkingPlace));

            content += "</table>";

            return content;
        }

        public static string GenerateTableHead()
        {
            return $@"<tr>
                    <th>Parking Lot</th>
                    <th>Available Spaces</th> 
                    <th>All Spaces</th>
                    <th>Available Spaces</th>
                  </tr>";
        }

        public static string GenerateEmailRow(ParkingPlaceDTO parkingPlace)
        {
            var occupationRate =
                GetAvailableSpacePercent(parkingPlace.AllParkingPlaces, parkingPlace.FreeParkingPlaces);

            return $@"
                  <tr>
                    <td>{parkingPlace.Name}</td>
                    <td>{parkingPlace.FreeParkingPlaces}</td> 
                    <td>{parkingPlace.AllParkingPlaces}</td>
                    <td>{occupationRate}</td>
                  </tr>";
        }

        public static string GetAvailableSpacePercent(double all, double free)
        {
            var availablePercent = 0.0;

            if (all > 0 & free > 0)
            {
                availablePercent = free / (all / 100);
            }

            if (availablePercent < 10) return $"<p style=\"color:#990000\"><b>{availablePercent:F}%</b></p>";

            if (availablePercent < 30) return $"<p style=\"color:#ff8000\"><b>{availablePercent:F}%</b></p>";

            return $"<p style=\"color:#32cd32\"><b>{availablePercent:F}%</b></p>";
        }

        public static string GenerateAd()
        {
            return @"<img src=""cid:AdImage""/>";
        }   
    }
}
    