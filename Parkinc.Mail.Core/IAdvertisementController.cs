﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Parkinc.Mail.Core
{
    public interface IAdvertisementController
    {
        //todo: method signature
        Task<string> Fetch();
    }
}
