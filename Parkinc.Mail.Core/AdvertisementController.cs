﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Parkinc.Mail.Core.Communication;

namespace Parkinc.Mail.Core
{
    public class AdvertisementController : IAdvertisementController
    {
        private readonly ICommunication _communication;

        public AdvertisementController(ICommunication communication)
        {
            _communication = communication;
        }

        public async Task<string> Fetch()
        {
            const string url = "http://api.parkinc.valorl.com/ads/ad";
            return await _communication.GetRequestAsync(url);
        }
    }
}
    