﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Parkinc.Mail.DTOs;
using Parkinc.Mail.Models;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Parkinc.Mail.Core
{
    public class EmailController : IEmailController
    {
        private readonly ISendGridClient _emailClient;
        private readonly IAdvertisementController _adController;

        public EmailController(ISendGridClient emailClient, IAdvertisementController adController)
        {
            _emailClient = emailClient;
            _adController = adController;
        }
         
        public async Task<Response> Send(EmailModel email)
        {
            if (email.IsValid == false)
            {
                throw new ArgumentException("Email objet is not valid.");
            }

            var message = email.GetSendGridMessage();

            if (email.WithAds)
            {
                var source = await _adController.Fetch();
                message.AddAttachment("file.png", source, "image/png", "inline", "AdImage");
            }

            return await _emailClient.SendEmailAsync(message);
        }

        public string GenerateEmailContent(IEnumerable<ParkingPlaceDTO> parkingPlaces, bool withAds)
        {
            var header = EmailContent.GenerateHeader();
            var table = EmailContent.GenerateTable(parkingPlaces);
            var footer = EmailContent.GenerateFooter();
            var ad = "";

            if (withAds)
            {
                ad = EmailContent.GenerateAd();
            }

            return header + table + ad + footer;
        }
    }
}
