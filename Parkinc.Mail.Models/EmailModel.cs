﻿using System.Text.RegularExpressions;

namespace Parkinc.Mail.Models
{
    public class EmailModel
    {
        private readonly Regex _emailRegex;

        public EmailModel()
        {
            _emailRegex =
                new Regex(
                    @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        public string Sender { get; set; }
        public string Reciever { get; set; }
        public string Content { get; set; }
        public string Subject { get; set; }
        public bool WithAds { get; set; }
        public string AdId { get; set; }

        public bool HasValidReciever => _emailRegex.IsMatch(Reciever);
        public bool HasValidSender => _emailRegex.IsMatch(Sender);
        public bool IsValid => HasValidReciever && HasValidSender;
    }
}