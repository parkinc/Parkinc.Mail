﻿using SendGrid.Helpers.Mail;

namespace Parkinc.Mail.Models
{
    public static class EmailModelExtentions
    {
        public static EmailModel WithReciever(this EmailModel model, string reciever)
        {
            model.Reciever = reciever;
            return model;
        }

        public static EmailModel WithSender(this EmailModel model, string sender)
        {
            model.Sender = sender;
            return model;
        }

        public static EmailModel WithContent(this EmailModel model, string content)
        {
            model.Content = content;    
            return model;
        }

        public static SendGridMessage GetSendGridMessage(this EmailModel model)
        {
            var sender = new EmailAddress(model.Sender);
            var reciever = new EmailAddress(model.Reciever);

            var msg = new SendGridMessage()
            {
                From = sender,
                Subject = model.Subject,
                //PlainTextContent = model.Content,
                HtmlContent = model.Content
            };

            msg.AddTo(reciever);
            return msg;
        }
    }
}
