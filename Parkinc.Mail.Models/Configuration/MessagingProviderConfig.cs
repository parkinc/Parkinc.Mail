﻿using System.Collections.Generic;

namespace Parkinc.Mail.Models.Configuration
{
    public class MessagingProviderConfig
    {
        public string ConnectionString { get; set; }
        public IEnumerable<EndpointConfig> Endpoints { get; set; }
    }
}
