﻿namespace Parkinc.Mail.Models.Configuration
{
    public class EmailProviderConfig
    {
        public string ApiKey { get; set; }
        public string SenderEmail { get; set; }
    }
}
