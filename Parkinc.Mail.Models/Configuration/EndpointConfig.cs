﻿namespace Parkinc.Mail.Models.Configuration
{
    public class EndpointConfig
    {
        public string Name { get; set; }
        public string SubscriptionId { get; set; }
    }
}