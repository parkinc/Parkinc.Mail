﻿using System.Collections.Generic;
using Parkinc.Mail.Service.Endpoints;

namespace Parkinc.Mail.Service.Manager
{
    public interface IEndpointManager
    {
        void AddEndpoint(IEndpoint endpoint);
        void AddEndpoints(IEnumerable<IEndpoint> endpoints);
        void StartAll();
        void StopAll();
    }
}
