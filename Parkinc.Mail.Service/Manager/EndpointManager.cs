﻿using System;
using System.Collections.Generic;
using EasyNetQ;
using Parkinc.Mail.Service.Endpoints;

namespace Parkinc.Mail.Service.Manager
{
    public class EndpointManager : IEndpointManager
    {
        private readonly IBus _bus;
        private readonly IList<IEndpoint> _endpoints;

        public EndpointManager(IBus bus)
        {
            _bus = bus;
            _endpoints = new List<IEndpoint>();
        }

        public EndpointManager(IEnumerable<IEndpoint> endpoints, IBus bus)
        {
            _bus = bus;
            _endpoints = new List<IEndpoint>();
            AddEndpoints(endpoints);
        }

        public void AddEndpoint(IEndpoint endpoint)
        {
            endpoint.Setbus(_bus);
            _endpoints.Add(endpoint);
        }

        public void AddEndpoints(IEnumerable<IEndpoint> endpoints)
        {
            foreach (var endpoint in endpoints)
            {
                AddEndpoint(endpoint);
            }
        }

        public void StartAll()
        {
            var count = 0;

            foreach (var endpoint in _endpoints)
            {
                endpoint.Start();
                count++;
            }

            if (count == 0)
            {
                throw new Exception("There must be at least one endpoint registered in the endpoint manager.");
            }
        }

        public void StopAll()
        {
            var count = 0;

            foreach (var endpoint in _endpoints)
            {
                endpoint.Stop();
                count++;
            }

            if (count == 0)
            {
                throw new Exception("There must be at least one endpoint registered in the endpoint manager.");
            }
        }

        public IEnumerable<IEndpoint> Endpoints => _endpoints;
    }
}
