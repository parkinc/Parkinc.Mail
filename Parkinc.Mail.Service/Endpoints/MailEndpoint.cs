﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EasyNetQ;
using Microsoft.Extensions.Options;
using Parkinc.Mail.Core;
using Parkinc.Mail.DTOs;
using Parkinc.Mail.Models;
using Parkinc.Mail.Models.Configuration;
using SendGrid;

namespace Parkinc.Mail.Service.Endpoints
{
    public class MailEndpoint : IEndpoint
    {
        private IBus _bus;
        private ISubscriptionResult _subscription;
        private readonly IEmailController _emailController;
        private readonly IOptions<EmailProviderConfig> _emalOptions;
        private readonly IOptions<MessagingProviderConfig> _messagingOptions;
        private bool _isRunning;

        public MailEndpoint(IEmailController emailController, IOptions<EmailProviderConfig> emailOptions,
            IOptions<MessagingProviderConfig> messagingOptions)
        {
            _isRunning = false;
            _emailController = emailController;
            _emalOptions = emailOptions;
            _messagingOptions = messagingOptions;
        }

        public MailEndpoint(IEmailController emailController, IOptions<EmailProviderConfig> emailOptions,
            IOptions<MessagingProviderConfig> messagingOptions, IBus bus)
        {
            _isRunning = false;
            _emailController = emailController;
            _emalOptions = emailOptions;
            _messagingOptions = messagingOptions;
            _bus = bus;
        }

        public void Start()
        {
            if (_bus == null)
            {
                throw new NullReferenceException("Bus cannot be null.");
            }

            _isRunning = true;

            SubscribeAsync();
        }

        public void Stop()
        {
            _subscription.SafeDispose();
            _isRunning = false;
        }

        public bool IsRunning()
        {
            return _isRunning;
        }

        public void Setbus(IBus bus)
        {
            _bus = bus;
        }

        public string SubscriptionId
        {
            get
            {
                var endpoints = _messagingOptions.Value.Endpoints;
                var endpoint = endpoints.First(x => x.Name == GetType().Name);
                return endpoint.SubscriptionId;
            }
        }

        private void SubscribeAsync()
        {
            _subscription = _bus.SubscribeAsync<MailDTO>(SubscriptionId, async message => 
            {
                var content = _emailController.GenerateEmailContent(message.ParkingPlaces, message.WithAds);
                var mail = CreateEmail(message, content);
                await SendMail(mail);
            });
        }

        private Task<Response> SendMail(EmailModel mail)
        {
            return Task.Run(() => _emailController.Send(mail))
                .ContinueWith(task =>
                {
                    if (task.IsCompleted == false || task.IsFaulted)
                    {
                        throw new EasyNetQException("Messaging exception sent to the default error queue.");
                    }

                    return task.Result;
                });
        }

        private EmailModel CreateEmail(MailDTO message, string content)
        {
            return new EmailModel
            {
                Sender = _emalOptions.Value.SenderEmail,
                Reciever = message.RecieverEmail,
                Subject = "Parkinc - Parking lots summary",
                Content = content,
                WithAds = message.WithAds   
            };
        }
    }
}
