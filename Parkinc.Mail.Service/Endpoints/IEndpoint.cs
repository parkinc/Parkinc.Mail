﻿using EasyNetQ;

namespace Parkinc.Mail.Service.Endpoints
{
    public interface IEndpoint
    {
        void Start();
        void Stop();
        bool IsRunning();

        void Setbus(IBus bus);
    }
}
