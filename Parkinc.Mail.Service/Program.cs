﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Parkinc.Mail.Core;
using Parkinc.Mail.Service.Factories;
using Parkinc.Mail.Service.Manager;
using IServiceProvider = System.IServiceProvider;

namespace Parkinc.Mail.Service
{
    class Program
    {
        private static IConfiguration _configuration;
        private static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {
            Startup();
        
            var endpointFactory = _serviceProvider.GetService<IEndpointFactory>();
            var endpoints = endpointFactory.GetEndpoints();

            var endpointManager = _serviceProvider.GetService<IEndpointManager>();

            endpointManager.AddEndpoints(endpoints);
            endpointManager.StartAll();
        }

        private static void Startup()
        {
            Console.WriteLine("Starting mail service...");
            _configuration = Configure.AppSettings("appsettings.json");
            _serviceProvider = Configure.Dependencies(_configuration);
        }
    }
}
