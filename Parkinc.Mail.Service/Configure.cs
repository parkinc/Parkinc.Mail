﻿using System;
using System.IO;
using EasyNetQ;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Parkinc.Mail.Core;
using Parkinc.Mail.Core.Communication;
using Parkinc.Mail.Models.Configuration;
using Parkinc.Mail.Service.Factories;
using Parkinc.Mail.Service.Manager;
using SendGrid;

namespace Parkinc.Mail.Service
{
    public static class Configure
    {
        public static IConfiguration AppSettings(string filePath)
        {
            Console.WriteLine("Current DIR:" + Directory.GetCurrentDirectory());
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(filePath, true, true);

            return builder.Build();
        }

        public static ServiceProvider Dependencies(IConfiguration configuration)
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IEmailController, EmailController>()
                .AddSingleton<IAdvertisementController, AdvertisementController>()
                .AddSingleton<ISendGridClient, SendGridClient>(provider =>
                    new SendGridClient(configuration["EmailProvider:ApiKey"]))
                .AddSingleton<IEndpointFactory, EndpointFactory>()
                .AddSingleton<IEndpointManager, EndpointManager>()
                .AddScoped<ICommunication, Communication>()
                .AddSingleton(RabbitHutch.CreateBus(configuration["MessagingProvider:ConnectionString"]))
                .Configure<EmailProviderConfig>(configuration.GetSection("EmailProvider"))
                .Configure<MessagingProviderConfig>(configuration.GetSection("MessagingProvider"))
                .AddOptions()
                .BuildServiceProvider();

            return serviceProvider;
        }
    }
}