﻿using System.Collections.Generic;
using EasyNetQ;
using Parkinc.Mail.Service.Endpoints;
using Parkinc.Mail.Service.Manager;

namespace Parkinc.Mail.Service.Factories
{
    public interface IEndpointFactory
    {
        IEnumerable<IEndpoint> GetEndpoints();
    }
}
