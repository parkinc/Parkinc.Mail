﻿using System;
using System.Collections.Generic;
using EasyNetQ;
using Microsoft.Extensions.Options;
using Parkinc.Mail.Core;
using Parkinc.Mail.Models.Configuration;
using Parkinc.Mail.Service.Endpoints;
using Parkinc.Mail.Service.Manager;

namespace Parkinc.Mail.Service.Factories
{
    public class EndpointFactory : IEndpointFactory
    {
        private readonly IEmailController _emailController;
        private readonly IOptions<EmailProviderConfig> _emailOptions;
        private readonly IOptions<MessagingProviderConfig> _messagingOptions;

        public EndpointFactory(IEmailController emailController, IOptions<EmailProviderConfig> emailOptions, IOptions<MessagingProviderConfig> messagingOptions)
        {
            _emailController = emailController;
            _emailOptions = emailOptions;
            _messagingOptions = messagingOptions;
        }

        public IEnumerable<IEndpoint> GetEndpoints() => new List<IEndpoint>()
        {
            new MailEndpoint(_emailController, _emailOptions, _messagingOptions)
        };
    }
}
