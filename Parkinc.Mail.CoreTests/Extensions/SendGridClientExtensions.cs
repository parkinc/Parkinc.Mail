﻿using System.Net;
using System.Threading;
using Moq;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Parkinc.Mail.CoreTests.Extensions
{
    public static class SendGridClientExtensions
    {
        public static Mock<ISendGridClient> WithAcceptedSendResponse(this Mock<ISendGridClient> mock)
        {
            mock.Setup(x => x.SendEmailAsync(It.IsAny<SendGridMessage>(), It.IsAny<CancellationToken>())).ReturnsAsync(new Response(HttpStatusCode.Accepted, null, null));
            return mock;
        }

        public static Mock<ISendGridClient> WithServerErrorSendResponse(this Mock<ISendGridClient> mock)
        {
            mock.Setup(x => x.SendEmailAsync(It.IsAny<SendGridMessage>(), It.IsAny<CancellationToken>())).ReturnsAsync(new Response(HttpStatusCode.InternalServerError, null, null));
            return mock;
        }
    }
}
