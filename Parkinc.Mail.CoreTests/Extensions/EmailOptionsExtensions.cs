﻿using Microsoft.Extensions.Options;
using Moq;
using Parkinc.Mail.Models.Configuration;

namespace Parkinc.Mail.CoreTests.Extensions
{
    public static class EmailOptionsExtensions
    {
        public static Mock<IOptions<EmailProviderConfig>> WithValue(this Mock<IOptions<EmailProviderConfig>> model,
            EmailProviderConfig value)
        {
            model.Setup(x => x.Value).Returns(value);
            return model;
        }
    }
}
